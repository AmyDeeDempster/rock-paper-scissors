#!/usr/bin/env perl
use v5.12;
use open ':std', ':encoding(UTF-8)';
use utf8;
use Modern::Perl;
use String::Similarity;
use Readonly;
Readonly::Scalar our $VERSION => 2;

# Booleans.
use constant TRUE  => 1;
use constant FALSE => 0;
use constant ME    => 1;    # This corresponds to $i_won being TRUE.
use constant YOU   => 0;

# Game vocabulary.
Readonly::Array our @CHOICES => qw(Rock Paper Scissors);
Readonly::Array our @VICTORY => qw(blunts wraps cuts);

main();

sub main {
	print 'Welcome to the game of ';
	print join q{-}, @CHOICES;
	say ".\n\nType your move.";

	my @score = ( 0, 0 );
	while (<>) {
		chomp;
		my $human = name_to_num($_);
		unless ( defined $human ) {
			say 'Quitting.';
			last;
		}
		my $computer = int rand @CHOICES;
		printf( "You play %s, and I play %s.\n",
			$CHOICES[$human], $CHOICES[$computer], );

		# Have we again come to blows fruitlessly, noble foe?
		if ( $human == $computer ) {
			say 'That is a stalemate.';
			next;
		}

		# I hope you haven’t used this one.
		my $kryptonite = ( $computer + 1 ) % @CHOICES;

		# Have I defeated my cursed human adversary?
		my $i_won = ( $human == $kryptonite ) ? FALSE : TRUE;

		print $i_won ? 'Hehe!' : 'Curses!';

		# The aftermath.
		my $what_worked = $i_won ? $computer : $human;
		printf( " %s %s %s.\n",
			$CHOICES[$what_worked], $VICTORY[$what_worked],
			$CHOICES[ ( $what_worked - 1 ) % @CHOICES ] );

		$score[$i_won]++;
	}
	print "At the end of play, I have won $score[ME] rounds, "
		. "and you have won $score[YOU].\nWell done";
	say( ( $score[YOU] > $score[ME] ) ? q{.} : q{, me!} );
	exit $score[YOU] - $score[ME];
}

sub name_to_num {
	my $input = uc;
	Readonly my $THRESHOLD => 0.2;
	my ( $max, $best, $current, $option ) = 0;

	for ( keys @CHOICES ) {
		$option = uc $CHOICES[$_];
		$current = similarity( $input, $option );
		if ( $current > $max ) {
			$max  = $current;
			$best = $_;
		}
	}
	$best = undef unless ( $max > $THRESHOLD );
	return $best;
}

__END__

=pod

=encoding utf8

=head1 NAME

Rock-Paper-Scissors

=head1 SYNOPSIS

Run the script and follow the prompts
(entering ‘rock’, ‘paper’ or ‘scissors’, abbreviating if you like).

Type ‘quit’, or just the Enter key, or Ctrl-D to quit the game.

If you prefer, you can pass the game your
pre-determined list of moves from a file:

C<./rps.pl moves.txt>

Or via a pipe:

C<echo -e "rock \n rock \n scis \n pap \n sciss" | ./rps.pl>

=head1 DESCRIPTION

This is the classic game of
Rock-Paper-Scissors,
Paper-Scissors-Stone,
Zhot, or Roshambo.

Play against the randomly-moving computer player.
At the end, your score is displayed.

=head1 DEPENDENCIES

C<use Modern::Perl;>

C<use String::Similarity;>

C<use Readonly;>

=head1 EXIT STATUS

Returns the net number of rounds you have won
(negative if you have lost more than won).

=head1 AUTHOR

Amy Dee Dempster.

=head1 LICENCE AND COPYRIGHT

© 2018 Amy Dee Dempster.
For academic purposes only.

=cut
